using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Random = UnityEngine.Random;

public class BoneManagerScript : MonoBehaviour
{
    [SerializeField] private InputField numberOfPoints;
    [SerializeField] private Button generatePointsButton;
    [SerializeField] private Button clearEverythingButton;
    [SerializeField] private Button constructBonesButton;
    [SerializeField] private GameObject spherePrefab;
    [SerializeField] private GameObject barycenterPrefab;
    [SerializeField] private float spawnAreaWidth;
    [SerializeField] private float spawnAreaHeight;
    [SerializeField] private float spawnAreaDepth;
    [SerializeField] private List<GameObject> bodyParts;

    private List<GameObject> generatedPoints;
    private List<GameObject> generatedLines;
    private List<Vector3> generatedPointsPositions;

    private float[][] covarianceMatrix;
    private float[] eigenVectors;
    private float[] eigenValues;
    public bool pointsFromMesh;
    private List<Vector3> lowerBonesPoints;
    private List<Vector3> upperBonesPoints;
    private GameObject G;   // Barycentre

    private List<List<Vector3>> allBodyPartsMeshPositions;
    private readonly List<Vector3> headMeshPositions;
    private readonly List<Vector3> chestMeshPositions;
    private readonly List<Vector3> rightArmMeshPositions;
    private readonly List<Vector3> rightForearmMeshPositions;
    private readonly List<Vector3> rightHandMeshPositions;
    private readonly List<Vector3> leftArmMeshPositions;
    private readonly List<Vector3> leftForeamMeshPositions;
    private readonly List<Vector3> leftHandMeshPositions;
    private readonly List<Vector3> rightLegMeshPositions;
    private readonly List<Vector3> rightFootMeshPositions;
    private readonly List<Vector3> leftLegMeshPositions;
    private readonly List<Vector3> leftFootMeshPositions;

    // Initilise les données
    void Start()
    {
        numberOfPoints.text = "15";
        generatePointsButton.onClick.AddListener(GeneratePoints);
        constructBonesButton.onClick.AddListener(MainAlgorithm);
        clearEverythingButton.onClick.AddListener(ClearGeneratedPoints);

        generatedPoints = new List<GameObject>();
        generatedLines = new List<GameObject>();
        generatedPointsPositions = new List<Vector3>();
        allBodyPartsMeshPositions = new List<List<Vector3>>
        {
            headMeshPositions,
            chestMeshPositions,
            rightArmMeshPositions,
            rightForearmMeshPositions,
            rightHandMeshPositions,
            leftArmMeshPositions,
            leftForeamMeshPositions,
            leftHandMeshPositions,
            rightLegMeshPositions,
            rightFootMeshPositions,
            leftLegMeshPositions,
            leftFootMeshPositions
        };
        for (int i = 0; i < allBodyPartsMeshPositions.Count; ++i)
            allBodyPartsMeshPositions[i] = new List<Vector3>();

        covarianceMatrix = new float[3][];
        covarianceMatrix[0] = new float[3];
        covarianceMatrix[1] = new float[3];
        covarianceMatrix[2] = new float[3];
        eigenVectors = new float[3];
        eigenValues = new float[1];
        lowerBonesPoints = new List<Vector3>();
        upperBonesPoints = new List<Vector3>();
    }

    // Renseigne les points associés à chaque mesh
    void GetPointsFromMesh()
    {
        for(int i = 0; i <allBodyPartsMeshPositions.Count; ++i)
        {
            allBodyPartsMeshPositions[i].Clear();
            foreach (var vert in bodyParts[i].GetComponent<MeshFilter>().mesh.vertices)
                allBodyPartsMeshPositions[i].Add(bodyParts[i].transform.TransformPoint(vert));
        }
    }

    // Supprime les points générés
    void ClearGeneratedPoints()
    {
        foreach(var point in generatedPoints)
            Destroy(point);
        generatedPoints.Clear();
        generatedPointsPositions.Clear();
        Destroy(G);
        if (generatedLines.Count > 0)
            ClearGeneretadLines();
    }

    // Efface les lignes générées
    void ClearGeneretadLines()
    {
        foreach (var line in generatedLines)
            Destroy(line);
        generatedLines.Clear();
    }

    // Génére un certain nombre de points aléatoires
    void GeneratePoints() 
    {
        ClearGeneratedPoints();
        int.TryParse(numberOfPoints.text, out int numberOfPointsToGenerate);
        for (int i = 0; i < numberOfPointsToGenerate; ++i)
        {
            float randX = Random.Range(-spawnAreaWidth, spawnAreaWidth);
            float randY = Random.Range(-spawnAreaHeight, spawnAreaHeight);
            float randZ = Random.Range(0, spawnAreaDepth);
            GameObject pointSphere = Instantiate(spherePrefab, new Vector3(randX, randY, randZ), Quaternion.identity);
            generatedPoints.Add(pointSphere);
            generatedPointsPositions.Add(generatedPoints[i].gameObject.transform.position);
        }
    }

    // Calcul du barycentre
    void BarycenterComputation(List<Vector3> listOfPoints)
    {
        float xSum = 0, ySum = 0, zSum = 0;
        foreach(var point in listOfPoints)
        {
            xSum += point.x;
            ySum += point.y;
            zSum += point.z;
        }
        float numberOfPoints = listOfPoints.Count;
        G = Instantiate(barycenterPrefab, new Vector3(xSum / numberOfPoints, ySum / numberOfPoints, zSum / numberOfPoints), Quaternion.identity);
    }

    // Calcul de la covariance
    float CovarianceComputation(List<float> l1, List<float> l2, List<Vector3> ptList)
    {
        float sumL1 = 0, sumL2 = 0, sumL1xL2 = 0;
        for (int i = 0; i < ptList.Count; ++i) {
            sumL1 += l1[i];
            sumL2 += l2[i];
            sumL1xL2 += l1[i] * l2[i];
        }
        sumL1 /= (float)ptList.Count;
        sumL2 /= (float)ptList.Count;
        sumL1xL2 /= (float)ptList.Count;
        return sumL1xL2 - sumL1 * sumL2;
    }

    // Calcul de la matrice de covariance
    void CovarianceMatrixComputation(List<Vector3> listOfPoints)
    {
        List<float> X = new List<float>(), Y = new List<float>(), Z = new List<float>();
        foreach(var point in listOfPoints)
        {
            X.Add(point.x);
            Y.Add(point.y);
            Z.Add(point.z);
        }
        covarianceMatrix[0][0] = CovarianceComputation(X, X, listOfPoints);
        covarianceMatrix[0][1] = covarianceMatrix[1][0] = CovarianceComputation(X, Y, listOfPoints);
        covarianceMatrix[0][2] = covarianceMatrix[2][2] = CovarianceComputation(X, Z, listOfPoints);
        covarianceMatrix[1][1] = CovarianceComputation(Y, Y, listOfPoints);
        covarianceMatrix[1][2] = covarianceMatrix[2][1] = CovarianceComputation(Y, Z, listOfPoints);
        covarianceMatrix[2][2] = CovarianceComputation(Z, Z, listOfPoints);
    }

    // Calcul la plus grande valeur propre et le vecteur propre associé
    void PowerIteration()
    {
        float[] computationVector = new float[3];
        int maxIteration = 0;
        eigenValues[0] = 0;
        eigenVectors[0] = Random.Range(-10.0f, 10.0f);
        eigenVectors[1] = Random.Range(-10.0f, 10.0f);
        eigenVectors[2] = Random.Range(-10.0f, 10.0f);

        do
        {
            maxIteration++;
            for (int i = 0; i < 3; i++)
            {
                computationVector[i] = 0.0f;
                for (int j = 0; j < 3; j++)
                    computationVector[i] += covarianceMatrix[i][j] * eigenVectors[j];
            }
            for (int i = 0; i < 3; i++)
                eigenVectors[i] = computationVector[i];
            float norm = Mathf.Sqrt(Mathf.Pow(eigenVectors[0], 2) + Mathf.Pow(eigenVectors[1], 2) + Mathf.Pow(eigenVectors[2], 2));
            eigenVectors[0] /= norm;
            eigenVectors[1] /= norm;
            eigenVectors[2] /= norm;
        } while (maxIteration < 100000);
    }

    // Calcul les deux points limites d'un bones à l'aide de la composante principale
    void ComputePrincipalComponent(List<Vector3> listOfPoints)
    {
        float minValue = +1000000;
        float maxValue = -1000000;

        foreach(var point in listOfPoints)
        {
            float sum = 0;
            sum += eigenVectors[0] * (point.x - G.transform.position.x);
            sum += eigenVectors[1] * (point.y - G.transform.position.y);
            sum += eigenVectors[2] * (point.z - G.transform.position.z);
            if (sum < minValue)
                minValue = sum;
            else if (sum > maxValue)
                maxValue = sum;
        }

        var minPoint = new Vector3
        {
            x = G.transform.position.x + minValue * eigenVectors[0],
            y = G.transform.position.y + minValue * eigenVectors[1],
            z = G.transform.position.z + minValue * eigenVectors[2]
        };

        var maxPoint = new Vector3
        {
            x = G.transform.position.x + maxValue * eigenVectors[0],
            y = G.transform.position.y + maxValue * eigenVectors[1],
            z = G.transform.position.z + maxValue * eigenVectors[2]
        };

        if (minPoint.y > maxPoint.y) {
            upperBonesPoints.Add(minPoint);
            lowerBonesPoints.Add(maxPoint);
        } else {
            lowerBonesPoints.Add(minPoint);
            upperBonesPoints.Add(maxPoint);
        }
    }

    // Dessine une ligne blanche représentant le bone d'un mesh
    void DrawBone()
    {
        Debug.DrawLine(lowerBonesPoints[lowerBonesPoints.Count - 1], upperBonesPoints[upperBonesPoints.Count - 1], UnityEngine.Color.white, 9001);
    }

    // Dessine une ligne rouge reliant deux bones qui devraient être connectés
    void DrawArticulations()
    {
        var point1 = lowerBonesPoints[0]; // Head
        var point2 = upperBonesPoints[1]; // Chest
        Debug.DrawLine(point1, point2, UnityEngine.Color.red, 9001);

        point1 = upperBonesPoints[2]; // Right Arm
        point2 = upperBonesPoints[1]; // Chest
        Debug.DrawLine(point1, point2, UnityEngine.Color.red, 9001);

        point1 = lowerBonesPoints[2]; // Right Arm
        point2 = upperBonesPoints[3]; // Right Forearm
        Debug.DrawLine(point1, point2, UnityEngine.Color.red, 9001);

        point1 = lowerBonesPoints[3]; // Right Forearm
        point2 = upperBonesPoints[4]; // Right Hand
        Debug.DrawLine(point1, point2, UnityEngine.Color.red, 9001);

        point1 = upperBonesPoints[5]; // Left Arm
        point2 = upperBonesPoints[1]; // Chest
        Debug.DrawLine(point1, point2, UnityEngine.Color.red, 9001);

        point1 = lowerBonesPoints[5]; // Left Arm
        point2 = upperBonesPoints[6]; // Left Forearm
        Debug.DrawLine(point1, point2, UnityEngine.Color.red, 9001);

        point1 = lowerBonesPoints[6]; // Left Forearm
        point2 = upperBonesPoints[7]; // Left Hand
        Debug.DrawLine(point1, point2, UnityEngine.Color.red, 9001);

        point1 = lowerBonesPoints[8]; // Right Leg
        point2 = upperBonesPoints[9]; // Right Foot
        Debug.DrawLine(point1, point2, UnityEngine.Color.red, 9001);

        point1 = lowerBonesPoints[10]; // Left Leg
        point2 = upperBonesPoints[11]; // Left Foot
        Debug.DrawLine(point1, point2, UnityEngine.Color.red, 9001);

        point1 = upperBonesPoints[8]; // Right Leg
        point2 = lowerBonesPoints[1]; // Chest
        Debug.DrawLine(point1, point2, UnityEngine.Color.red, 9001);

        point1 = upperBonesPoints[10]; // Left Leg
        point2 = lowerBonesPoints[1]; // Chest
        Debug.DrawLine(point1, point2, UnityEngine.Color.red, 9001);
    }

    // Algorithme principal lancant l'enchainement des fonctions
    void MainAlgorithm() {
        lowerBonesPoints.Clear();
        upperBonesPoints.Clear();
        if (pointsFromMesh && generatedPoints.Count == 0) {
            GetPointsFromMesh();
            foreach(var bodyPart in allBodyPartsMeshPositions)
            { 
                BarycenterComputation(bodyPart);
                CovarianceMatrixComputation(bodyPart);
                PowerIteration();
                ComputePrincipalComponent(bodyPart);
                DrawBone();
            }
            DrawArticulations();
        }
        else if (pointsFromMesh && generatedPoints.Count > 0)
            Debug.Log("Please clear points to get points from mesh");
    }
}
